
## Documention

## Errorlog Module

Following are the files which are related to the Errorlog Module &the required Common Files for Errorlog module to execute individually.


## Controllers :
-	ErrorLogReportController.php
-	ExportController -> Common File for Export functionality


## Base:
-	ErrorLogBaseController.php


## Controller_services :
-	ErrorLogServiceController.php


##  Model Folder:
-	ErrorlogModel.php
-	MasterModel.php -> Functions (insertData,  queryBinder) 


## Database/Migrations :
- 	error_logs.php


## Views:
-	Layout  :This layouts is common layouts use in ErrorLog Module.
-	app.blade.php
-	coming_soon.blade.php
-	filter.blade.php
-	listing-container.blade.php
-	listing.blade.php
-	search.blade.php
-	pagination.blade.php

-	errors : This views use in ErrorLog Module.
-	oh!.blade.php



## Common Folder:
•	common.php -> error_logging () function .This function is  shift to the     	 			               ErrorLogBaseController.php  file.

## Export Folder: 
-	Export.php  ->files is common file used in errorlog Module .

## CSS Used for ErrorLog Module:
-	app.css
-	menu.css
-	bootstrap .min .css
-	orange_theme.css
-	chosen.css
-	daterangepicker.css
-	accordion.css


## JS used for ErrorLog Module: 
-	jquery.min.js
-	common.js
-	jquery.nestable.js
-	menu.js
-	bootstrap.min.js
-	chosen.jquery.js
-	moment.min.js
-	accordion.js
-	daterangepicker.min.js
-	tiny.min.js.


## Fonts :
•	font-awesome.min.js













