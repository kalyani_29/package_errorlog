<?php

namespace App\Package\ErrorLog\src\controllers;

use App\Exports\Export;
use App\Package\ErrorLog\src\controllers\Controller_services\ErrorLogServiceController;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp;
use Illuminate\Support\Facades\Session;
use App\Package\ErrorLog\src\controllers\Base\ErrorLogBaseController;

class ErrorLogReportController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $requestData = $request->all();
        //dd($requestData);
        try {

            $responseData = ErrorLogServiceController::errolog_summary_listing($request);
            // dd($responseData);
            //separation for compact
            $listingData    = $responseData['listingData'];
            $paginationData = $responseData['paginationData'];
            $filterFormData = $responseData['filterFormData'];
            $filterFields   = $responseData['filterFields'];
            $settings       = $responseData['settings'];
            $headers        = $responseData['headers'];
            $addEditFormKey = $responseData['addEditFormKey'];

            if ($request->ajax()) {
                return view('Errorlog::layouts.listing', compact('headers', 'listingData', 'paginationData', 'settings', 'addEditFormKey', 'filterFormData', 'filterFields'));
            }
            return view('Errorlog::layouts.listing-container', compact('headers', 'listingData', 'paginationData', 'settings', 'addEditFormKey', 'filterFormData', 'filterFields'));

        }catch (Exception $ex)
        {
            $ErrorlogBaseController = new ErrorLogBaseController();
            $ErrorlogBaseController->error_logging($ex, 'index', 'ErrorLogReportController.php');
            return view('Errorlog::errors.oh!');
        }
    }


    public function errorlog_report(Request $request)
    {
        $requestData = $request->all();

        try {
            $responseData = ErrorLogServiceController::errolog_details_listing($request);
            //dd($responseData );
            //separation for compact
            $listingData    = $responseData['listingData'];
            $paginationData = $responseData['paginationData'];
            $filterFormData = $responseData['filterFormData'];
            $filterFields   = $responseData['filterFields'];
            $settings       = $responseData['settings'];
            $headers        = $responseData['headers'];
            $addEditFormKey = $responseData['addEditFormKey'];

            if ($request->ajax()) {
                return view('Errorlog::layouts.listing', compact('headers', 'listingData', 'paginationData', 'settings', 'addEditFormKey', 'filterFormData', 'filterFields'));
            }
            return view('Errorlog::layouts.listing-container', compact('headers', 'listingData', 'paginationData', 'settings', 'addEditFormKey', 'filterFormData', 'filterFields'));

        } catch (Exception $ex)
        {
            // //dd($ex->getMessage());
            $ErrorlogBaseController = new ErrorLogBaseController();
            $ErrorlogBaseController->error_logging($ex,'errorlog_report', 'ErrorLogReportController.php');
            return view('Errorlog::errors.oh!');
        }

    }
}
