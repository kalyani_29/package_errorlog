<?php

namespace App\Package\ErrorLog\src\controllers\Base;

use App\Exports\Export;
use App\Package\ErrorLog\src\models\MasterModel;
use Illuminate\Http\Request;
use App\Package\ErrorLog\src\controllers\Controller;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\DB;
use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ErrorLogBaseController extends Controller
{
    static function errorLogListing($request, $sessionData, $settings, $filterFormKey, $extraSettings)
    {
        $requestData = $request->all();
            //dd($requestData);
        try {
            $filterFormData = '';
            $filterFields = '';

            if (!empty($filterFormKey)) {
                // Swapnil 19-Feb-21
                // $filterFormData     = DB::table('forms')->where('form_key', '=', $filterFormKey)->first();
                // $filterFields       = DB::table('form_fields')->where('form_id', $filterFormData->id)->where('is_deleted', '=', '0')->orderBy('sequence', 'asc')->get();
                // $filterFormData     = CommanBaseController::get_form_info($filterFormKey);
                // $filterFields       = CommanBaseController::get_form_fields($filterFormData->id);
            }

            $query = DB::table('error_logs')
            ->select($extraSettings['select']);

            //dd($query);

            if (isset($requestData['basics']) && !empty($requestData['basics'])) {
                $query->where(function ($query) use ($requestData) {
                    $query->where('id', 'LIKE', '%' . $requestData['basics'] . '%')
                        ->orwhere('page', 'LIKE', '%' . $requestData['basics'] . '%')
                        ->orwhere('function', 'LIKE', '%' . $requestData['basics'] . '%')
                        ->orwhere('error_code', 'LIKE', '%' . $requestData['basics'] . '%');
                });
            }

            if (isset($extraSettings['group_by']) && !empty($extraSettings['group_by'])) {
                $query->groupBy($extraSettings['group_by']);
            }


            $query = MasterModel::queryBinder($extraSettings, $query);

            $settings['orderBy'] = 'id';
            $settings['sortOrder'] = 'ASC';

             /* Use for sorting data */
             if (isset($requestData['filter_column_name']) && isset($requestData['sorting_method']) && !empty($requestData['filter_column_name']) && !empty($requestData['sorting_method'])) {
                $settings['orderBy'] = $requestData['filter_column_name'];
                $settings['sortOrder'] = $requestData['sorting_method'];
            }

            /* Export */
            if ($settings['export_button'] == 1) {
                $export = new Export();
                $sql_query = $export->eloquentSqlWithBindings($query);
                $export->setExcelParameters($sql_query, $extraSettings['headers'], $settings['list_title']);
            }


            if (isset($extraSettings['pagination'])) {
                $returnData = $query->orderBy($settings['orderBy'], $settings['sortOrder'])->paginate($extraSettings['pagination']);
            } else {
                $returnData = $query->orderBy($settings['orderBy'], $settings['sortOrder'])->paginate(10)->withPath('errorlog_report')->appends(['date' => $requestData['date']]);
            }

            $paginationData = $returnData;

            return [
                'listingData'       => json_decode(json_encode($returnData), true),
                'paginationData'    => $paginationData,
                'filterFormData'    => $filterFormData,
                'filterFields'      => $filterFields,
                'settings'          => $settings,
                'headers'           => $extraSettings['headers']
            ];
        } catch (Exception $ex)
        {
            //object create errorlogBaseController and call the error_logging
            $ErrorlogBaseController = new ErrorLogBaseController();
            $ErrorlogBaseController->error_logging($ex,'errorLogListing', 'ErrorLogBaseController.php');
            return view('Errorlog::layouts.coming_soon');
        }
    }



    //Errorlog stored
    public function error_logging($e, $function, $file)
    {
        try {
            $userDetail = Auth::user();

            $userID = 0;
            $userName = '';
            if (!empty($userDetail)) {
                $userID = $userDetail->id;
                $userName = $userDetail->user_name;
            }

            $masterModel               = new MasterModel();
            $errorLogging['date']         = Carbon::now();//returns the current date and time
            $errorLogging['time']         = date('H:i:s');
            $errorLogging['user_id']      = $userID;
            $errorLogging['user_name']    = $userName;
            $errorLogging['page']         = $file;
            $errorLogging['function']     = $function;
            $errorLogging['error_code']   = $e->getCode();
            $errorLogging['description']  = $e->getMessage() . ' - ' . $e->getLine();

            // echo "<pre>";print_r($errorLogging);die;
            $rows_affected = $masterModel->insertData($errorLogging, 'error_logs');
            $errorString = "Error occur for user " .$userName .
             ", having user id " . $userID . " on  " . $file .
             "<br/> Following is error Message ." . $errorLogging['description'] .
              " having error code " . $errorLogging['error_code'];

            //$email = new Email();
            //$email->content_email(array('errors.sharedmachine@gmail.com'), 'Error Messages', 'Error Message', '', $errorString);

            return;

        } catch (Exception $ex)
        {
            //object create errorlogBaseController and call the error_logging
            $ErrorlogBaseController = new ErrorLogBaseController();
            $ErrorlogBaseController->error_logging($ex,'error_logging', 'ErrorLogBaseController.php');
            return view('Errorlog::layouts.coming_soon');
        }
    }
}
