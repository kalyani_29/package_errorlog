<?php
namespace App\Package\ErrorLog\src\controllers\Controller_services;

use App\Package\ErrorLog\src\controllers\Controller;
use Exception;
use App\Package\ErrorLog\src\controllers\Base\ErrorLogBaseController;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class ErrorLogServiceController extends Controller
{
    static function errolog_summary_listing($request)
    {
        $filterFormKey  = '';
        $addEditFormKey = '';
        $extraSettings = [];
        $sessionData ='';
      //  $sessionData    = auth()->user();
        try{
            $settings = [
                    'action_edit_button'        => '0',
                    'action_view_button'        => '0',
                    'action_accordion_button'   => '0',
                    'accordion_url'             => '',
                    'add_button'                => '0',
                    'pagination'                => '1',
                    'filter_button'             => '0',
                    'group_button'              => '0',
                    'export_button'             => '0',
                    'import_button'             => '0',
                    'search'                    => '1',
                    'list_title'                => 'Error Summary',
                    'action_header'             => '0',
                    'multiSaveKey'              => '',
                ];

                $extraSettings['headers'] = [
                        'rownum'               => 'Sr. No',
                        'date'                 => 'Date',
                        'count_link'           => 'No. of errors',

                    ];

                DB::statement(DB::raw('set @rownum=0'));

                $extraSettings['select'] = [
                    DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                    DB::raw('count(id) as error_count'),
                    DB::raw("CONCAT('<a class=pointer href=errorlog_report?date=',date,'>',getShortDate(date),'</a>') as date"),
                    DB::raw("CONCAT('<a class=pointer href=errorlog_report?date=',date,'>',count(id),'</a>') as count_link"),
                    'date as id'
                ];

                $extraSettings['group_by']   = ['date'];
                $extraSettings['pagination'] = 4;
                $responseData   = ErrorLogBaseController::errorLogListing($request, $sessionData, $settings, $filterFormKey, $extraSettings);

                $responseData['addEditFormKey']=$addEditFormKey;
                if (empty($responseData['headers'])) {
                    $responseData['headers'] = $extraSettings['headers'];
                }
            return $responseData;
        }catch(Exception $ex)
        {
            //create object of errorlogBaseController and call the error_logging
            $ErrorlogBaseController = new ErrorLogBaseController();
            $ErrorlogBaseController->error_logging($ex,'errolog_summary_listing', 'ErrorLogServiceController.php');
            return view('Errorlog::errors.oh!');
        }
    }




    static function errolog_details_listing($request)
    {
        $request['date'] = isset($request['date']) ? $request['date'] : Carbon::now()->toDateString();
        $requestData    = $request->all();

        $filterFormKey  = '';
        $addEditFormKey = '';
        $extraSettings  = [];
        $sessionData='';
        //$sessionData    = $request->session()->get('user_info');
        $date = Carbon::parse($requestData['date'])->format('d-M-Y');

        try{
            $settings = [
                    'action_edit_button'        => '0',
                    'action_view_button'        => '0',
                    'action_accordion_button'   => '0',
                    'accordion_url'             => '',
                    'add_button'                => '0',
                    'pagination'                => '1',
                    'filter_button'             => '0',//
                    'group_button'              => '0',
                    'export_button'             => '1',//
                    'import_button'             => '0',
                    'search'                    => '1',
                    'list_title'                => 'Error Log ('.$date .')',
                    'action_header'             => '0',
                    'multiSaveKey'              => '',

                ];

                $extraSettings['headers'] = [
                    'id'                        => 'Sr. No',
                    'date'                      => 'Date',
                    'page'                      => 'File Name',
                    'function'                  => 'Function',
                    'error_code'                => 'Error Code',
                    'description'               => 'Description',
                ];

                $extraSettings['select'] = [
                    'id',
                     'error_code',
                     DB::raw("getShortDate(date) as date"),
                     DB::raw('TIME(created_at) as date_tool_tip'),
                    'page',
                    'function',
                    'description'
                ];

                $extraSettings['where'] = [
                    'where' => [
                        ['column' => 'date', 'expression' => '=', 'value' => $requestData['date']]
                    ],
                ];
                $extraSettings['pagination_path_parameters'] = 'yes';
                $responseData   = ErrorLogBaseController::errorLogListing($request, $sessionData, $settings, $filterFormKey, $extraSettings);

                $responseData['addEditFormKey']=$addEditFormKey;
                if (empty($responseData['headers'])) {
                    $responseData['headers'] = $extraSettings['headers'];
                }
            return $responseData;

        }catch(Exception $ex)
        {
            //object create errorlogBaseController and call the error_logging
            $ErrorlogBaseController = new ErrorLogBaseController();
            $ErrorlogBaseController->error_logging($ex,'errolog_details_listing', 'ErrorLogServiceController.php');
            return view('Errorlog::errors.oh!');

        }
    }

}
