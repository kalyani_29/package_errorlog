<?php

use Illuminate\Support\Facades\Route;



Route::group(['namespace' => 'App\Package\Errorlog\src\controllers', /*'prefix' => 'auth',*/ 'middleware' => ['web']], function() {
    Route::get('errorlog_summary', 'ErrorLogReportController@index');
    Route::get('errorlog_report', 'ErrorLogReportController@errorlog_report');
    Route::get('export_to_excel', 'ExportController@export');


});
