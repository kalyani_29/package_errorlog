$(document).ready(function() {

    var updateOutput = function(e) {
        try {
            var list = e.length ? e : $(e.target),
                output = list.data('output');

            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                // dragReport($('#nestable').data('output', $('#nestable-output')));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        } catch (err) {
            for (var prop in err) {
                console.log("property: " + prop + " value: [" + err[prop] + "]\n");
            }
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
            group: 1
        })
        .on('change', updateOutput);

    /*Update menu mapping on drag*/
    $('#nestable').on('change', function() {
        /* on change event */
        console.log('change');
        dragReport($('#nestable').data('output', $('#nestable-output')));
    });

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));

    /*Delete menu mapping & there child menu*/
    $('#nestable .button-delete').on('click', function() {
        deleteFromMenu($(this).data('owner-id'), $(this).data('owner-name'));
    });

    /*Edit menu*/
    $('#nestable .button-edit').on('click', function() {
        console.log($(this).data('styles'));
        editFromMenu($(this).data('owner-id'),
            $(this).data('owner-name'),
            $(this).data('url'),
            $(this).data('shortcode'),
            $(this).data('menu-url'),
            $(this).data('font-class'),
            $(this).data('styles') // Added by Shrikant. SMWE-107 Dashboard Menus Images into One Single Image
        );
    });
});

/* Update Menu Mapping */
function dragReport(e) {
    var list = e.length ? e : $(e.target),
        output = list.data('output');

    if (window.JSON) {
        if (output) {
            output.val(window.JSON.stringify(list.nestable('serialize')));
            var list = window.JSON.stringify(list.nestable('serialize'));
            var menu_name = getUrlParameter('menu_name');

            $.ajax({
                type: 'post',
                url: APP_URL + '/updateMenuList',
                data: { 'list': list, 'menu_name': menu_name },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    console.log(data)
                        //location.reload();
                }
            });
        }
    } else {
        alert('JSON browser support required for this page.');
    }
}

/*Delete menu mapping*/
function deleteFromMenu(id, name) {
    console.log('--delete---', id);
    console.log('===delete==', name);

    var result = confirm("Delete " + name + " and all its subitems ?");
    if (!result) {
        return;
    }

    $.ajax({
        type: 'GET',
        url: APP_URL + '/deleteMenuMappingAndRoles',
        data: { 'targetId': id },
        success: function(data) {
            location.reload();
        }
    });
};

/*Edit menu*/
function editFromMenu(id, name, url, short_code, menu_url, font_class, styles) {
    $("#menu-name").html(name);
    $("#old_id").val(id);
    $("#short_code").val(short_code);
    $("#description").val(name);
    $("#url").val(url);
    $("#menu_url").val(menu_url);
    $("#font_icon_class").val(font_class);
    // Added by Shrikant. SMWE-107 Dashboard Menus Images into One Single Image
    $("#menu_styles").val(styles);
};
