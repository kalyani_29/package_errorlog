// this js used by accordion.blade.php

$(document).ready(function() {
    // Check Accordion is Expanded or not.
    // Added by Shrikant 29-Dec-20
    var isExpandedAccordion = localStorage.getItem("isExpandedAccordion");
    if (isExpandedAccordion == "no") {
        noExpandedjs();
    }
    if (isExpandedAccordion == "yes") {
        ExpandedJs();
    }

    function noExpandedjs() {
        $(".sidebar-labels").hide();
        $(".accordion-sidebar-title").css("height", "35px");
        $(".side-bar").css('max-width', '6.666667%');
        $(".right_panel").addClass('jq-right-panel-add').removeClass('jq-right-panel-remove');
        $(".side-bar").removeClass("open");
    }

    function ExpandedJs() {
        $(".accordion-sidebar-title").css('height', 'unset');
        $(".right_panel").removeClass('jq-right-panel-add').addClass('jq-right-panel-remove');
        $(".side-bar").css('max-width', '21%');
        $(".side-bar").addClass("open");
        $(".sidebar-labels").show();
    }
    // End of Accordion
    $('#collapse0').addClass('show'); /*-- Use for collapse only first accordion --*/
    //$('#heading0 div b i').removeClass("fa-angle-up").addClass("fa-angle-down");
    // Arrows on Accordion header. Added by Shrikant on 23-Jan-21 (SMWE-66)
    $(".accordion-start .collapse").each(function() {
        var id = $(this).attr('id');
        if ($("#" + id).hasClass('show')) {
            //$("#" + id).prev('.card-header').find(".fa").removeClass("fa-angle-up").addClass("fa-angle-down");
            $("#" + id).prev('.card-header').find(".fa-collapse").removeClass("fa-angle-down").addClass("fa-angle-up");
        } else {
            //$("#" + id).prev('.card-header').find(".fa").removeClass("fa-angle-down").addClass("fa-angle-up");
            $("#" + id).prev('.card-header').find(".fa-collapse").removeClass("fa-angle-up").addClass("fa-angle-down");
        }
    });

    // Toggle angle up icon on show hide of collapse element in accordion cards
    $(".collapse").on('show.bs.collapse', function() {
        //$(this).prev(".card-header").find(".fa").removeClass("fa-angle-up").addClass("fa-angle-down");
        $(this).prev(".card-header").find(".fa-collapse").removeClass("fa-angle-down").addClass("fa-angle-up");
        $(".card-header-button0").show();
        $(this).prev(".card-header").removeClass('border-bottom_0px');
    }).on('hide.bs.collapse', function() {
        //$(this).prev(".card-header").find(".fa").removeClass("fa-angle-down").addClass("fa-angle-up");
        $(this).prev(".card-header").find(".fa-collapse").removeClass("fa-angle-up").addClass("fa-angle-down");
        $(".card-header-button0").hide();
        $(this).prev(".card-header").addClass('border-bottom_0px');
    });

    /*--- Left side Menu ---*/
    $("#sidebarCollapse").click(function() {
        if ($(".side-bar").hasClass("open")) {
            localStorage.setItem("isExpandedAccordion", "no");
            noExpandedjs();
        } else {
            localStorage.setItem("isExpandedAccordion", "yes");
            ExpandedJs();
        }
    });

    $("div.sidebar-sticky ul li.nav-item").click(function() {
        $("div.sidebar-sticky ul li.nav-item").removeClass('orange-nav-item-active');
        $(this).addClass("orange-nav-item-active");
    });
});
